<?php
namespace STALKER_CMS\Packages\Imagine\Image;

use STALKER_CMS\Packages\Imagine\Image\Metadata\DefaultMetadataReader;
use STALKER_CMS\Packages\Imagine\Image\Metadata\ExifMetadataReader;
use STALKER_CMS\Packages\Imagine\Image\Metadata\MetadataReaderInterface;
use STALKER_CMS\Packages\Imagine\Exception\InvalidArgumentException;

abstract class AbstractImagine implements ImagineInterface {

    /** @var MetadataReaderInterface */
    private $metadataReader;

    /**
     * @param MetadataReaderInterface $metadataReader
     *
     * @return ImagineInterface
     */
    public function setMetadataReader(MetadataReaderInterface $metadataReader) {

        $this->metadataReader = $metadataReader;
        return $this;
    }

    /**
     * @return MetadataReaderInterface
     */
    public function getMetadataReader() {

        if(NULL === $this->metadataReader) {
            if(ExifMetadataReader::isSupported()) {
                $this->metadataReader = new ExifMetadataReader();
            } else {
                $this->metadataReader = new DefaultMetadataReader();
            }
        }
        return $this->metadataReader;
    }

    /**
     * Checks a path that could be used with ImagineInterface::open and returns
     * a proper string.
     *
     * @param string|object $path
     *
     * @return string
     *
     * @throws InvalidArgumentException In case the given path is invalid.
     */
    protected function checkPath($path) {

        // provide compatibility with objects such as \SplFileInfo
        if(is_object($path) && method_exists($path, '__toString')) {
            $path = (string)$path;
        }
        $handle = @fopen($path, 'r');
        if(FALSE === $handle) {
            throw new InvalidArgumentException(sprintf('File %s does not exist.', $path));
        }
        fclose($handle);
        return $path;
    }
}
