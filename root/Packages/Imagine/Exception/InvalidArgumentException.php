<?php
namespace STALKER_CMS\Packages\Imagine\Exception;

/**
 * Imagine-specific invalid argument exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements Exception {

}
