<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;

/**
 * A strip filter
 */
class Strip implements FilterInterface {

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->strip();
    }
}
