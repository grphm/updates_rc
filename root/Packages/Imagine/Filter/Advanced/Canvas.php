<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Advanced;

use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\BoxInterface;
use STALKER_CMS\Packages\Imagine\Image\Point;
use STALKER_CMS\Packages\Imagine\Image\PointInterface;
use STALKER_CMS\Packages\Imagine\Image\Palette\Color\ColorInterface;
use STALKER_CMS\Packages\Imagine\Image\ImagineInterface;

/**
 * A canvas filter
 */
class Canvas implements FilterInterface {

    /**
     * @var BoxInterface
     */
    private $size;
    /**
     * @var PointInterface
     */
    private $placement;
    /**
     * @var ColorInterface
     */
    private $background;
    /**
     * @var ImagineInterface
     */
    private $imagine;

    /**
     * Constructs Canvas filter with given width and height and the placement of the current image
     * inside the new canvas
     *
     * @param ImagineInterface $imagine
     * @param BoxInterface $size
     * @param PointInterface $placement
     * @param ColorInterface $background
     */
    public function __construct(ImagineInterface $imagine, BoxInterface $size, PointInterface $placement = NULL, ColorInterface $background = NULL) {

        $this->imagine = $imagine;
        $this->size = $size;
        $this->placement = $placement ? : new Point(0, 0);
        $this->background = $background;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        $canvas = $this->imagine->create($this->size, $this->background);
        $canvas->paste($image, $this->placement);
        return $canvas;
    }
}
