<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Advanced;

use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;
use STALKER_CMS\Packages\Imagine\Image\Point;

/**
 * The Grayscale filter calculates the gray-value based on RGB.
 */
class Grayscale extends OnPixelBased implements FilterInterface {

    public function __construct() {

        parent::__construct(function(ImageInterface $image, Point $point) {

            $color = $image->getColorAt($point);
            $image->draw()->dot($point, $color->grayscale());
        });
    }
}
