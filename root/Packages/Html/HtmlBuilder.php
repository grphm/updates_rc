<?php
namespace STALKER_CMS\Packages\Html;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Traits\Macroable;

class HtmlBuilder {

    use Macroable;
    protected $url;

    /**
     * HtmlBuilder constructor.
     * @param UrlGenerator|null $url
     */
    public function __construct(UrlGenerator $url = NULL) {

        $this->url = $url;
    }

    /**
     * Convert an HTML string to entities.
     *
     * @param  string $value
     * @return string
     */
    public function entities($value) {

        return htmlentities($value, ENT_QUOTES, 'UTF-8', FALSE);
    }

    /**
     * Convert entities to HTML characters.
     *
     * @param  string $value
     * @return string
     */
    public function decode($value) {

        return html_entity_decode($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Generate a link to a JavaScript file.
     *
     * @param  string $url
     * @param  array $attributes
     * @param  bool $secure
     * @return string
     */
    public function script($url, $attributes = array(), $secure = NULL) {

        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['src'] = $this->url->asset($url, $secure);
        return '<script'.$this->attributes($attributes).'></script>';
    }

    /**
     * Generate a link to a CSS file.
     *
     * @param  string $url
     * @param  array $attributes
     * @param  bool $secure
     * @return string
     */
    public function style($url, $attributes = array(), $secure = NULL) {

        $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');
        $attributes = $attributes + $defaults;
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['href'] = $this->url->asset($url, $secure);
        return '<link'.$this->attributes($attributes).'>';
    }

    /**
     * Генерирует meta-теги для отображения Favicon
     * @return string
     */
    public function favicon() {

        $html = NULL;
        if(\File::isDirectory(public_path('theme/favicon'))):
            $html = '<link rel="apple-touch-icon-precomposed" sizes="57x57" href="'.asset('theme/favicon/apple-touch-icon-57x57.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.asset('theme/favicon/apple-touch-icon-114x114.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.asset('theme/favicon/apple-touch-icon-72x72.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.asset('theme/favicon/apple-touch-icon-144x144.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="60x60" href="'.asset('theme/favicon/apple-touch-icon-60x60.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="120x120" href="'.asset('theme/favicon/apple-touch-icon-120x120.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="76x76" href="'.asset('theme/favicon/apple-touch-icon-76x76.png').'" />'."\n";
            $html .= "\t".'<link rel="apple-touch-icon-precomposed" sizes="152x152" href="'.asset('theme/favicon/apple-touch-icon-152x152.png').'" />'."\n";
            $html .= "\t".'<link rel="icon" type="image/png" href="'.asset('theme/favicon/favicon-196x196.png').'" sizes="196x196" />'."\n";
            $html .= "\t".'<link rel="icon" type="image/png" href="'.asset('theme/favicon/favicon-96x96.png').'" sizes="96x96" />'."\n";
            $html .= "\t".'<link rel="icon" type="image/png" href="'.asset('theme/favicon/favicon-32x32.png').'" sizes="32x32" />'."\n";
            $html .= "\t".'<link rel="icon" type="image/png" href="'.asset('theme/favicon/favicon-16x16.png').'" sizes="16x16" />'."\n";
            $html .= "\t".'<link rel="icon" type="image/png" href="'.asset('theme/favicon/favicon-128.png').'" sizes="128x128" />'."\n";
            $html .= "\t".'<meta name="application-name" content="&nbsp;"/>'."\n";
            $html .= "\t".'<meta name="msapplication-TileColor" content="#FFFFFF" />'."\n";
            $html .= "\t".'<meta name="msapplication-TileImage" content="'.asset('theme/favicon/mstile-144x144.png').'" />'."\n";
            $html .= "\t".'<meta name="msapplication-square70x70logo" content="'.asset('theme/favicon/mstile-70x70.png').'" />'."\n";
            $html .= "\t".'<meta name="msapplication-square150x150logo" content="'.asset('theme/favicon/mstile-150x150.png').'" />'."\n";
            $html .= "\t".'<meta name="msapplication-square310x310logo" content="'.asset('theme/favicon/mstile-310x310.png').'" />';
        elseif(\File::exists(public_path('favicon.ico'))):
            $html = '<link rel="shortcut icon" href="'.asset('favicon.ico').'" type="image/x-icon">'."\n";
            $html .= "\t".'<link rel="icon" href="'.asset('favicon.ico').'" type="image/x-icon">';
        endif;
        return $html;
    }

    /**
     * Генерирует meta-теги на основе переданных аттрибутов
     * @param array $attributes
     * @return string
     */
    public function meta($attributes = array()) {

        $defaults = array('name' => '', 'content' => '');
        $attributes = $attributes + $defaults;
        return '<meta'.$this->attributes($attributes).'>';
    }

    /**
     * Generate an HTML image element.
     *
     * @param  string $url
     * @param  string $alt
     * @param  array $attributes
     * @param  bool $secure
     * @return string
     */
    public function image($url, $alt = NULL, $attributes = array(), $secure = NULL) {

        $attributes['alt'] = $alt;
        return '<img src="'.$this->url->asset($url, $secure).'"'.$this->attributes($attributes).'>';
    }

    /**
     * Generate a HTML link.
     *
     * @param  string $url
     * @param  string $title
     * @param  array $attributes
     * @param  bool $secure
     * @return string
     */
    public function link($url, $title = NULL, $attributes = array(), $secure = NULL) {

        $url = $this->url->to($url, array(), $secure);
        if(is_null($title) || $title === FALSE) {
            $title = $url;
        }
        return '<a href="'.$url.'"'.$this->attributes($attributes).'>'.$this->entities($title).'</a>';
    }

    /**
     * Generate a HTTPS HTML link.
     *
     * @param  string $url
     * @param  string $title
     * @param  array $attributes
     * @return string
     */
    public function secureLink($url, $title = NULL, $attributes = array()) {

        return $this->link($url, $title, $attributes, TRUE);
    }

    /**
     * Generate a HTML link to an asset.
     *
     * @param  string $url
     * @param  string $title
     * @param  array $attributes
     * @param  bool $secure
     * @return string
     */
    public function linkAsset($url, $title = NULL, $attributes = array(), $secure = NULL) {

        $url = $this->url->asset($url, $secure);
        return $this->link($url, $title ? : $url, $attributes, $secure);
    }

    /**
     * Generate a HTTPS HTML link to an asset.
     *
     * @param  string $url
     * @param  string $title
     * @param  array $attributes
     * @return string
     */
    public function linkSecureAsset($url, $title = NULL, $attributes = array()) {

        return $this->linkAsset($url, $title, $attributes, TRUE);
    }

    /**
     * Generate a HTML link to a named route.
     *
     * @param  string $name
     * @param  string $title
     * @param  array $parameters
     * @param  array $attributes
     * @return string
     */
    public function linkRoute($name, $title = NULL, $parameters = array(), $attributes = array()) {

        return $this->link($this->url->route($name, $parameters), $title, $attributes);
    }

    /**
     * Generate a HTML link to a controller action.
     *
     * @param  string $action
     * @param  string $title
     * @param  array $parameters
     * @param  array $attributes
     * @return string
     */
    public function linkAction($action, $title = NULL, $parameters = array(), $attributes = array()) {

        return $this->link($this->url->action($action, $parameters), $title, $attributes);
    }

    /**
     * Generate a HTML link to an email address.
     *
     * @param  string $email
     * @param  string $title
     * @param  array $attributes
     * @return string
     */
    public function mailto($email, $title = NULL, $attributes = array()) {

        $email = $this->email($email);
        $title = $title ? : $email;
        $email = $this->obfuscate('mailto:').$email;
        return '<a href="'.$email.'"'.$this->attributes($attributes).'>'.$this->entities($title).'</a>';
    }

    /**
     * Obfuscate an e-mail address to prevent spam-bots from sniffing it.
     *
     * @param  string $email
     * @return string
     */
    public function email($email) {

        return str_replace('@', '&#64;', $this->obfuscate($email));
    }

    /**
     * Generate an ordered list of items.
     *
     * @param  array $list
     * @param  array $attributes
     * @return string
     */
    public function ol($list, $attributes = array()) {

        return $this->listing('ol', $list, $attributes);
    }

    /**
     * Generate an un-ordered list of items.
     *
     * @param  array $list
     * @param  array $attributes
     * @return string
     */
    public function ul($list, $attributes = array()) {

        return $this->listing('ul', $list, $attributes);
    }

    /**
     * Create a listing HTML element.
     *
     * @param  string $type
     * @param  array $list
     * @param  array $attributes
     * @return string
     */
    protected function listing($type, $list, $attributes = array()) {

        $html = '';
        if(count($list) == 0) {
            return $html;
        }
        foreach($list as $key => $value) {
            $html .= $this->listingElement($key, $type, $value);
        }
        $attributes = $this->attributes($attributes);
        return "<{$type}{$attributes}>{$html}</{$type}>";
    }

    /**
     * Create the HTML for a listing element.
     *
     * @param  mixed $key
     * @param  string $type
     * @param  string $value
     * @return string
     */
    protected function listingElement($key, $type, $value) {

        if(is_array($value)) {
            return $this->nestedListing($key, $type, $value);
        } else {
            return '<li>'.e($value).'</li>';
        }
    }

    /**
     * Create the HTML for a nested listing attribute.
     *
     * @param  mixed $key
     * @param  string $type
     * @param  string $value
     * @return string
     */
    protected function nestedListing($key, $type, $value) {

        if(is_int($key)) {
            return $this->listing($type, $value);
        } else {
            return '<li>'.$key.$this->listing($type, $value).'</li>';
        }
    }

    /**
     * Build an HTML attribute string from an array.
     *
     * @param  array $attributes
     * @return string
     */
    public function attributes($attributes) {

        $html = array();
        foreach((array)$attributes as $key => $value) {
            $element = $this->attributeElement($key, $value);
            if(!is_null($element)) {
                $html[] = $element;
            }
        }
        return count($html) > 0 ? ' '.implode(' ', $html) : '';
    }

    /**
     * Build a single attribute element.
     *
     * @param  string $key
     * @param  string $value
     * @return string
     */
    protected function attributeElement($key, $value) {

        if(is_numeric($key)) {
            $key = $value;
        }
        if(!is_null($value)) {
            return $key.'="'.e($value).'"';
        }
    }

    /**
     * Obfuscate a string to prevent spam-bots from sniffing it.
     *
     * @param  string $value
     * @return string
     */
    public function obfuscate($value) {

        $safe = '';
        foreach(str_split($value) as $letter) {
            if(ord($letter) > 128) {
                return $letter;
            }
            switch(rand(1, 3)) {
                case 1:
                    $safe .= '&#'.ord($letter).';';
                    break;
                case 2:
                    $safe .= '&#x'.dechex(ord($letter)).';';
                    break;
                case 3:
                    $safe .= $letter;
            }
        }
        return $safe;
    }

    /**
     * Подключает файл ресурса vendor CSS в зависимости от активной темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function coreVendorCSS($attributes = array(), $secure = NULL) {

        $theme = config('app.core_theme');
        $url = 'core/'.$theme.'/css/vendor.css';
        $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');
        $attributes = $attributes + $defaults;
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['href'] = $this->url->asset($url, $secure);
        return '<link'.$this->attributes($attributes).'>';
    }

    /**
     * Подключает файл ресурса main CSS в зависимости от активной темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function coreMainCSS($attributes = array(), $secure = NULL) {

        $theme = config('app.core_theme');
        $url = 'core/'.$theme.'/css/main.css';
        $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');
        $attributes = $attributes + $defaults;
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['href'] = $this->url->asset($url, $secure);
        return '<link'.$this->attributes($attributes).'>';
    }

    /**
     * Подключает файл ресурса vendor JS в зависимости от активной темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function coreVendorJS($attributes = array(), $secure = NULL) {

        $theme = config('app.core_theme');
        $url = 'core/'.$theme.'/js/vendor.js';
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['src'] = $this->url->asset($url, $secure);
        return '<script'.$this->attributes($attributes).'></script>';
    }

    /**
     * Подключает файл ресурса main JS в зависимости от активной темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function coreMainJS($attributes = array(), $secure = NULL) {

        $theme = config('app.core_theme');
        $url = 'core/'.$theme.'/js/main.js';
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['src'] = $this->url->asset($url, $secure);
        return '<script'.$this->attributes($attributes).'></script>';
    }

    /**************************************************************************/
    /**
     * Подключает файл ресурса vendor CSS из каталога темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function vendorCSS($attributes = array(), $secure = NULL) {

        $url = config('app.home_theme').'/styles/vendor.css';
        $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');
        $attributes = $attributes + $defaults;
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['href'] = $this->url->asset($url, $secure);
        return '<link'.$this->attributes($attributes).'>';
    }

    /**
     * Подключает файл ресурса main CSS из каталога темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function mainCSS($attributes = array(), $secure = NULL) {

        $url = config('app.home_theme').'/styles/main.css';
        $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');
        $attributes = $attributes + $defaults;
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['href'] = $this->url->asset($url, $secure);
        return '<link'.$this->attributes($attributes).'>';
    }

    /**
     * Подключает файл ресурса vendor JS из каталога темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function vendorJS($attributes = array(), $secure = NULL) {

        $url = config('app.home_theme').'/scripts/vendor.js';
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['src'] = $this->url->asset($url, $secure);
        return '<script'.$this->attributes($attributes).'></script>';
    }

    /**
     * Подключает файл ресурса main JS из каталога темы
     * @param array $attributes
     * @param null $secure
     * @return string
     */
    public function mainJS($attributes = array(), $secure = NULL) {

        $url = config('app.home_theme').'/scripts/main.js';
        if($source_file = realpath(public_path($url))):
            $url .= '?v='.filemtime($source_file);
        endif;
        $attributes['src'] = $this->url->asset($url, $secure);
        return '<script'.$this->attributes($attributes).'></script>';
    }
}