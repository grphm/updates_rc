<?php
namespace STALKER_CMS\Vendor\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * Регистрация artisan-команд
 * @package STALKER_CMS\Vendor\Console
 */
class Kernel extends ConsoleKernel {

    public function __construct(Application $app, Dispatcher $events) {

        $this->getHomeConsoleCommands();
        parent::__construct($app, $events);
    }

    protected $commands = [
        Commands\Uninstall::class,
        Commands\Package::class,
        Commands\Update::class,
        Commands\CacheKiller::class
    ];

    protected function schedule(Schedule $schedule) {
    }

    /********************************************************************************/
    /**
     * Получает и объеденяет artisan комманды из пакета
     * расширения функционала
     * @return $this
     */
    private function getHomeConsoleCommands() {

        $home_commands = [];
        $directory = base_path('home/Console/Commands');
        if(file_exists($directory)):
            if($scanned_directory = glob($directory.'/*.php')):
                foreach($scanned_directory as $file_name):
                    $home_commands[] = '\APPLICATION_HOME\Console\Commands\\'.basename($directory.'/'.$file_name, ".php");
                endforeach;
            endif;
        endif;
        $this->commands = array_merge($this->commands, $home_commands);
        return $this;
    }
}
