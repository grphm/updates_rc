<?php
return [
    'format_not_supported' => 'The format is not supported',
    'valid_size' => 'The uploaded file exceeds the allowable size. <br> Maximum size'
];