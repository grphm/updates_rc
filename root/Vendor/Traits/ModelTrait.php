<?php
namespace STALKER_CMS\Vendor\Traits;

use Carbon\Carbon;

/**
 * Модель
 * Class ModelTrait
 * @package STALKER_CMS\Vendor\Traits
 */
trait ModelTrait {

    /**
     * Возвращает человеческую дату создание записи в таблице
     * @return string
     */
    public function getCreatedDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    /**
     * Возвращает человеческую дату обновления записи в таблице
     * @return string
     */
    public function getUpdatedDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    }
}