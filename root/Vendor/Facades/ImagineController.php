<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ImagineController
 * Фасад контроллера по управлению пакетами CMS
 * @package STALKER_CMS\Vendor\Facades
 */
class ImagineController extends Facade {

    protected static function getFacadeAccessor() {

        return 'ImagineController';
    }
}