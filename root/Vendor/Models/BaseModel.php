<?php
namespace STALKER_CMS\Vendor\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Базовая модель CMS
 * Class BaseModel
 * @package STALKER_CMS\Vendor\Models
 */
abstract class BaseModel extends Model {

    /**
     * Возвращает модель или вызывает исключение
     * @param array $attributes
     * @return mixed
     * @throws Exception
     */
    public static function whereFind(array $attributes) {

        if(!is_null($instance = static::where($attributes)->first())):
            return $instance;
        else:
            throw (new ModelNotFoundException())->setModel(get_called_class());
        endif;
    }

    /**
     * Проверяет существуют записи в модели
     * @param array $attributes
     * @return mixed
     */
    public static function whereExist(array $attributes) {

        return static::where($attributes)->exists();
    }

    /**
     * Проверяет уникальность записи в модели
     * @param array $attributes
     * @param null $id
     * @throws \Exception
     */
    public static function uniqueness(array $attributes, $id = NULL) {

        if(is_null($id) && static::where($attributes)->exists()):
            throw new \Exception(trans('root_lang::codes.2505'), 2505);
        elseif(is_numeric($id) && static::where('id', '!=', $id)->where($attributes)->exists()):
            throw new \Exception(trans('root_lang::codes.2505'), 2505);
        endif;
    }

    /**
     * Возвращает названия русских месяцев в дате
     * @param string $field
     * @param string $format
     * @param bool $genitive
     * @return mixed|null
     */
    public function DateRussianFormat($field = 'updated_at', $format = 'd F Y', $genitive = FALSE) {

        $months = [
            'January' => ['Январь', 'Января'], 'February' => ['Февраль', 'Февраля'],
            'March' => ['Март', 'Марта'], 'April' => ['Апрель', 'Апреля'],
            'May' => ['Май', 'Мая'], 'June' => ['Июнь', 'Июня'],
            'July' => ['Июль', 'Июля'], 'August' => ['Август', 'Августа'],
            'September' => ['Сентябрь', 'Сентября'], 'October' => ['Октябрь', 'Октября'],
            'November' => ['Ноябрь', 'Ноября'], 'December' => ['Декабрь', 'Декабря'],
            'Jan' => ['Янв', 'Янв'], 'Feb' => ['Фев', 'Фев'], 'Mar' => ['Март', 'Март'],
            'Apr' => ['Апр', 'Апр'], 'May' => ['Май', 'Май'], 'Jun' => ['Июн', 'Июн'],
            'Jul' => ['Июл', 'Июл'], 'Aug' => ['Авг', 'Авг'], 'Sep' => ['Сен', 'Сен'],
            'Oct' => ['Окт', 'Окт'], 'Nov' => ['Ноя', 'Ноя'], 'Dec' => ['Дек', 'Дек'],
        ];
        if(isset($this->attributes[$field])):
            $carbon = Carbon::parse($this->attributes[$field])->format($format);
            foreach($months as $en_month => $rus_month):
                if(strstr($carbon, $en_month)):
                    $replace = $rus_month[0];
                    if($genitive):
                        $replace = $rus_month[1];
                    endif;
                    return str_replace($en_month, $replace, $carbon);
                endif;
            endforeach;
        endif;
        return NULL;
    }
}