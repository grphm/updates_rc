<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

/**
 * Контроллера системой валидации
 * Class ValidatorController
 * @package STALKER_CMS\Vendor\Controllers
 * Контроллер проверки ввода
 */
class ValidatorController extends \Validator {

    /**
     * Метод проверки воода
     * @param \Request $request - Пользовательские данные
     * @param array $rules - Масиив ролей
     * @return bool
     */
    public function passes(\Request $request, array $rules) {

        $validator = parent::make($request::all(), $rules);
        return $validator->fails() ? FALSE : TRUE;
    }
}