<?php
namespace STALKER_CMS\Core\Dictionaries\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера словаря
 * Class Dictionary
 * @package STALKER_CMS\Core\Dictionaries\Facades
 */
class Dictionary extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicDictionariesController';
    }
}