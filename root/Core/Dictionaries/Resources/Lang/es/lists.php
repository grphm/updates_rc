<?php
return [
    'search' => 'Introduce el nombre de la lista de elementos',
    'structure_empty' => 'La estructura del diccionario está vacía',
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar elemento de diccionario',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'empty' => 'Lista está vacía',
    'module_disabled' => 'Módulo no está activo',
    'variable_not_set' => 'El valor no está definido',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de una entrada de diccionario',
        'form' => [
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Edición de la entrada de diccionario',
        'form' => [
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ]
];