<?php
return [
    'search' => 'Введите название элемента списка',
    'structure_empty' => 'Структура словаря пуста',
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить элемент словаря',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'module_disabled' => 'Модуль не активен',
    'variable_not_set' => 'Значение не определено',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление элемента словаря',
        'form' => [
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование элемента словаря',
        'form' => [
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ]
];