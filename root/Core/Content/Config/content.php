<?php
return [
    'package_name' => 'core_content',
    'package_title' => ['ru' => 'Модуль контента', 'en' => 'Content module', 'es' => 'Módulo de contenido'],
    'package_icon' => 'zmdi zmdi-view-quilt',
    'relations' => [],
    'menu_types' => [
        'page' => ['ru' => 'Шаблон страницы', 'en' => 'Page template', 'es' => 'Plantilla de página'],
        'menu' => ['ru' => 'Шаблон меню', 'en' => 'Menu template', 'es' => 'Modelo del menú'],
        'other' => ['ru' => 'Другие шаблоны', 'en' => 'Other templates', 'es' => 'Otras plantillas'],
    ],
    'forbidden_urls' => ['login', 'admin', 'install'],
    'version' => [
        'ver' => 0.7,
        'date' => '12.01.2017'
    ]
];
