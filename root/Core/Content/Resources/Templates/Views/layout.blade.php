<!DOCTYPE html>
<html lang="" class="no-js">
<head>
    {!! Html::meta(['charset' => 'utf-8', 'content' => NULL, 'name' => NULL]) !!}
    {!! Html::meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']) !!}
    <title>@yield('title', config('app.application_name'))</title>
    <meta name="description" content="@yield('description')">
    @yield('OpenGraph')
    {!! Html::meta(['name' => 'csrf-token', 'content' => csrf_token()]) !!}
    {!! Html::meta(['name' => 'locale', 'content' => \App::getLocale()]) !!}
    {!! Html::meta(['name' => 'fallback_locale', 'content' => config('app.fallback_locale')]) !!}
    {!! Html::vendorCSS() !!}
    {!! Html::mainCSS() !!}
    {!! Html::favicon() !!}
    @yield('head')
</head>
<body>
@yield('header')
@yield('content')
@yield('footer')
{!! Html::vendorJS() !!}
{!! Html::mainJS() !!}
@yield('scripts')
</body>
</html>