@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.content.menu.index') !!}">
                <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_content_lang::menu.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-plus"></i> @lang('core_content_lang::menu.insert.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => 'core.content.menu.store', 'class' => 'form-validate', 'id' => 'add-content-menu-form']) !!}
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_content_lang::menu.insert.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_content_lang::menu.insert.form.template')</p>
                        {!! Form::select('template_id', $menu_templates, NULL, ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group input-group fg-float input-group-help-block">
                        <div class="fg-line p-0 l-0 w-full">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input', 'autocomplete' => 'off']) !!}
                            <label class="fg-label">@lang('core_content_lang::menu.insert.form.slug')</label>
                        </div>
                        <span class="input-group-addon last bgm-white">
                            <button type="button" id="js-generate" class="btn btn-primary btn-icon waves-effect waves-circle waves-float">
                                <i class="zmdi zmdi-flash f-16"></i>
                            </button>
                        </span>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_content_lang::menu.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(function () {
            $("#js-generate").click(function () {
                $("input[name='slug']").str_random();
                $("input[name='slug']").parent().addClass('fg-toggled');
                $("input[name='slug']").parents('.input-group-help-block').removeClass('has-error').addClass('has-success').find('.help-block').remove();
            });
        });
    </script>
@stop