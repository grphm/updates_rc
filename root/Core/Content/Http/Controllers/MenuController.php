<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use STALKER_CMS\Core\Content\Models\Menu;
use STALKER_CMS\Core\Content\Models\MenuItem;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер меню
 * Class MenuController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class MenuController extends ModuleController implements CrudInterface {

    /**
     * @var Menu
     */
    protected $model;
    protected $locale_prefix;
    /**
     * @var PageTemplate
     */
    protected $templates;

    /**
     * MenuController constructor.
     * @param Menu $menu
     * @param PageTemplate $templates
     */
    public function __construct(Menu $menu, PageTemplate $templates) {

        parent::__construct();
        $this->model = $menu;
        $this->templates = $templates;
        $this->middleware('auth');
        \PermissionsController::allowPermission('core_content', 'menu');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index() {

        $menu_templates = $this->templates->whereLocale(\App::getLocale())->whereMenuType('menu')->get();
        if($menu_templates->count()):
            $menus = $this->model->whereLocale(\App::getLocale())->get();
            return view('core_content_views::menu.index', compact('menu_templates', 'menus'));
        else:
            return redirect()->route('core.content.templates.create')->with('status', 2613);
        endif;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $menu_templates = $this->templates->whereLocale(\App::getLocale())->whereMenuType('menu')->lists('title', 'id');
        return view('core_content_views::menu.create', compact('menu_templates'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $menu = $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.content.menu.items_index', $menu->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        $menu = $this->model->findOrFail($id);
        $menu_templates = $this->templates->whereLocale(\App::getLocale())->whereMenuType('menu')->lists('title', 'id');
        return view('core_content_views::menu.edit', compact('menu', 'menu_templates'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.content.menu.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {


        \RequestController::isAJAX()->init();
        MenuItem::whereMenuId($id)->delete();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.content.menu.index'))->json();
    }
}