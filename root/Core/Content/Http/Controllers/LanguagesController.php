<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class LanguagesController extends ModuleController implements CrudInterface {

    protected $locale_prefix;
    protected $languages_path;

    public function __construct() {

        parent::__construct();
        $this->languages_path = realpath(base_path('home/Resources/Lang/'.\App::getLocale()));
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('core_content', 'languages');
        $languages_files = new Collection(\File::glob($this->languages_path.'/*.php', GLOB_NOSORT));
        if($languages_files->count()):
            if(\Request::has('file') && \File::exists($this->languages_path.'/'.\Request::get('file').'.php')):
                $languages_content = \File::get($this->languages_path.'/'.\Request::get('file').'.php');
                $language_file = $this->languages_path.'/'.\Request::get('file').'.php';
            else:
                $languages_content = \File::get($languages_files->first());
                $language_file = $languages_files->first();
            endif;
            return view('core_content_views::languages.index', compact('languages_files', 'languages_content', 'language_file'));
        else:
            return redirect()->to(route('core.content.pages.index').'?status=2615');
        endif;
    }

    public function create() {
        // TODO: Implement create() method.
    }

    public function store() {

        \PermissionsController::allowPermission('core_content', 'languages');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, ['file' => 'required', 'content' => 'required'])):
            $file_path = double_slash($this->languages_path.'/'.$request::input('file'));
            \File::put($file_path, $request::input('content'));
            \Artisan::call('CacheKiller');
            return \ResponseController::success(202)->redirect(route('core.content.languages.index').'?file='.basename($file_path, '.php'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {
        // TODO: Implement edit() method.
    }

    public function update($id) {
        // TODO: Implement update() method.
    }

    public function destroy($id) {
        // TODO: Implement destroy() method.
    }
}