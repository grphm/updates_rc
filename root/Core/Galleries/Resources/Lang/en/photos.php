<?php
return [
    'breadcrumb' => 'Pictures',
    'title' => 'Pictures',
    'select_all' => 'Select all',
    'delete_selected' => [
        'question' => 'Remove selected images?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete selected',
    ],
    'delete_all' => [
        'question' => 'Delete all images?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete all',
    ],
    'edit' => 'Edit',
    'select' => 'Select',
    'cancel_selection' => 'Cancel selection',
    'delete' => [
        'question' => 'Delete images',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'dropzone' => [
        'title' => 'Upload gallery images',
        'description_formats' => 'Supported formats: ',
        'description_size' => 'Maximum upload size: ',
        'dictDefaultMessage' => 'Drop files here to upload',
        'dictFallbackMessage' => "Your browser does not support drag'n'drop file uploads",
        'dictFallbackText' => 'Please use the fallback form below to upload your files like in the olden days',
        'dictInvalidFileType' => 'The file does not match the allowed file types',
        'dictFileTooBig' => 'File is too big',
        'parallelUploads' => 'Not more than 50 simultaneous downloads'
    ],
    'form' => [
        'size' => 'Size',
        'type' => 'Type',
        'upload' => 'Upload',
        'uploading' => 'Uploading file',
        'done' => 'done',
    ],
    'modal_edit' => [
        'title' => 'Title',
        'alt_text' => 'Alt text',
        'submit' => 'Save',
        'cancel' => 'Cancel'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'Size is not defined'],
    'mime_undefined' => 'Mime-type is not defined'
];