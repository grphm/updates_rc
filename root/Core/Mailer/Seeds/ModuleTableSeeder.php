<?php
namespace STALKER_CMS\Core\Mailer\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('mailer_templates')->insert([
            'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Обратная связь', 'en' => 'Feedback', 'es' => 'Retroalimentación']),
            'path' => 'feedback.blade.php', 'required' => TRUE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('mailer_templates')->insert([
            'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Регистрация пользователя', 'en' => 'User registration', 'es' => 'Registro de usuarios']),
            'path' => 'registration.blade.php', 'required' => TRUE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        if(\DB::table('content_pages_templates')->exists()):
            \DB::table('content_pages_templates')->insert([
                'menu_type' => 'other', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Форма обратной связи', 'en' => 'Feedback form', 'es' => 'Feedback form']),
                'path' => 'feedback-form.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
            ]);
        endif;
    }

    private function translate(array $trans) {

        return array_first($trans, function($key, $value) {

            return $key == \App::getLocale();
        });
    }
}