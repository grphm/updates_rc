<?php
namespace STALKER_CMS\Core\Seo\Traits;

/**
 * Class SeoTrait
 * @package STALKER_CMS\Core\Seo\Traits
 */
trait SeoTrait {

    /**
     * Получить title страницы
     * @return mixed
     */
    public function getPageTitleAttribute() {

        if(!empty($this->attributes['seo_title'])):
            return $this->attributes['seo_title'];
        else:
            return $this->attributes['title'];
        endif;
    }

    /**
     * Получить description страницы
     * @return string
     */
    public function getPageDescriptionAttribute() {

        if(!empty($this->attributes['seo_description'])):
            return $this->attributes['seo_description'];
        else:
            return '';
        endif;
    }

    /**
     * Получить key_words страницы
     * @return mixed
     */
    public function getPageKeyWordsAttribute() {

        if(!empty($this->attributes['seo_keywords'])):
            return $this->attributes['seo_keywords'];
        else:
            return '';
        endif;
    }

    /**
     * Получить h1 страницы
     * @return mixed
     */
    public function getPageH1Attribute() {

        if(!empty($this->attributes['seo_h1'])):
            return $this->attributes['seo_h1'];
        else:
            return $this->attributes['title'];
        endif;
    }

    /**
     * Получить url страницы
     * @return mixed
     */
    public function getPageUrlAttribute() {

        if(!empty($this->attributes['seo_url'])):
            return $this->attributes['seo_url'];
        else:
            return $this->attributes['id'];
        endif;
    }

    /**
     * Проверка уникальности URL страницы
     * @param $url
     * @param null $id
     * @throws \Exception
     */
    public function uniqueSeoURL($url, $id = NULL) {

        if(!empty($url)):
            if(in_array($url, config('core_content::config.forbidden_urls'))):
                throw new \Exception(trans('root_lang::codes.2507'), 2507);
            elseif(is_null($id) && static::whereLocale(\App::getLocale())->where('seo_url', $url)->exists()):
                throw new \Exception(trans('root_lang::codes.2507'), 2507);
            elseif(is_numeric($id) && static::where('id', '!=', $id)->whereLocale(\App::getLocale())->where('seo_url', $url)->exists()):
                throw new \Exception(trans('root_lang::codes.2507'), 2507);
            endif;
        endif;
    }

    /**
     * Запрещает создавать зарезервированные URL
     * и URL как число
     * @param $url
     * @throws \Exception
     */
    public function forbiddenURL($url) {

        if(!empty($url)):
            if(in_array($url, config('core_content::config.forbidden_urls'))):
                throw new \Exception(\Lang::get('root_lang::codes.2507'), 2507);
            endif;
            if(is_numeric($url)):
                throw new \Exception(\Lang::get('root_lang::codes.2508'), 2508);
            endif;
        endif;
    }
}