<?php
return [
    'package_name' => 'core_uploads',
    'package_title' => ['ru' => 'Модуль загрузки', 'en' => 'Module uploads', 'es' => 'Los archivos subidos módulo'],
    'package_icon' => 'zmdi zmdi-upload',
    'relations' => [],
    'package_description' => [
        'ru' => 'Позволяет загружать файлы на сервер',
        'en' => 'It allows you to upload files to the server',
        'es' => 'Se le permite subir archivos al servidor'
    ],
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
