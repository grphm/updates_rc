{!! Form::open(['route' => 'core.uploads.store', 'class' => 'js-upload-form', 'files' => TRUE]) !!}
{!! Form::file('file', ['id' => 'js-upload-file', 'class' => 'hidden']) !!}
<div class="list-group">
    <div class="list-group-item media p-0">
        <div class="pull-left js-file-preview"></div>
        <div class="media-body">
            <div class="lgi-heading">
                <span id="js-file-name"></span>
            </div>
            <small class="lgi-text c-red js-file-error hidden"></small>
            <ul class="lgi-attrs">
                <li>@lang('core_uploads_lang::uploads.form.size'): <span id="js-file-size"></span></li>
                <li>@lang('core_uploads_lang::uploads.form.type'): <span id="js-file-type"></span></li>
            </ul>
            <div class="m-t-20">
                {!! Form::button(trans('core_uploads_lang::uploads.form.upload'), ['type' => 'submit', 'class' => 'btn btn-warning hidden']) !!}
                <div class="js-upload-progress hidden">
                    <p>
                        @lang('core_uploads_lang::uploads.form.uploading')
                        <span class="js-percent-complete">0</span> @lang('core_uploads_lang::uploads.form.done')
                        <button type="button" class="close xhr-close font-xs c-red pull-right">
                            <i class="zmdi zmdi-close"></i>
                        </button>
                    </p>
                    <div class="progress progress-striped progress-sm no-padding">
                        <div style="width: 0%;" role="progressbar" class="progress-bar bg-color-blueLight"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}