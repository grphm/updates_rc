<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Core\System\Models\Group;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Управление группами пользователей
 * Class GroupsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class GroupsController extends ModuleController implements CrudInterface {

    protected $model;

    /**
     * GroupsController constructor.
     * @param \STALKER_CMS\Core\System\Models\Group $groups
     */
    public function __construct(Group $groups) {

        $this->model = $groups;
        \PermissionsController::allowPermission('core_system', 'groups');
        $this->middleware('auth');
    }

    /**
     * Список доступных групп
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        return view('core_system_views::groups.index', ['groups' => $this->model->where('id', '>', 1)->with('users')->get()]);
    }

    /**
     * Добавление группы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_system_views::groups.create');
    }

    /**
     * Сохранение группы
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->uniqueness($request::only('slug'));
            $this->model->insert($request);
            if($request::has('new_interface')):
                try {
                    $new_interface_path = base_path('home/resources/views/'.strtolower($request::input('slug')));
                    $default_interface_path = realpath(__DIR__.'/../../Resources/Interfaces/default');
                    if(\File::exists($default_interface_path)):
                        \File::copyDirectory($default_interface_path, $new_interface_path);
                    endif;
                } catch(\Exception $e) {
                }
            endif;
            return \ResponseController::success(201)->redirect(route('core.system.groups.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование группы
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        return view('core_system_views::groups.edit', ['group' => $this->model->findOrFail($id)]);
    }

    /**
     * Обновление группы
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->uniqueness($request::only('slug'), $id);
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.system.groups.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление группы
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        try {
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.system.groups.index'))->json();
        } catch(\Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }
}