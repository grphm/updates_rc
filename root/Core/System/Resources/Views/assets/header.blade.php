<header id="header" class="clearfix" data-ma-theme="blue">
    <ul class="h-inner">
        <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="hi-logo hidden-xs">
            <a href="{!! route('dashboard') !!}">{!! config('app.application_name') !!}</a>
        </li>
        <li class="pull-right">
            <ul class="hi-menu">
                @if(!$CONFIG_CACHED)
                    <li>
                        <span class="badge bgm-red c-white f-16 p-10">@lang('core_system_lang::dashboard.header.config_no_cached')</span>
                    </li>
                @endif
                @if(settings(['core_system', 'settings', 'debug_mode']))
                    <li>
                        <span class="badge bgm-orange c-white f-16 p-10">@lang('core_system_lang::dashboard.header.debug_mode')</span>
                    </li>
                @endif
                @if(settings(['core_system', 'settings', 'services_mode']))
                    <li>
                        <span class="badge bgm-red c-white f-16 p-10">@lang('core_system_lang::dashboard.header.services_mode')</span>
                    </li>
                @endif
                @if(\PermissionsController::allowPermission('core_mailer', 'mailer', FALSE))
                    @set($notifications, \PublicMailer::getNewMessages())
                    @if($notifications->count())
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="">
                                <i class="him-icon zmdi zmdi-email"></i>
                                <i class="him-counts">{!! $notifications->count() !!}</i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg pull-right">
                                <div class="list-group">
                                    <div class="lg-header">
                                        @lang('core_system_lang::dashboard.header.notification')
                                    </div>
                                    <div class="lg-body">
                                        @foreach($notifications as $index => $notification)
                                            @if($index >= 5)
                                                @break()
                                            @endif
                                            <a class="list-group-item media" href="{!! route('core.mailer.index') !!}">
                                                <div class="pull-left">
                                                    @ProfileAvatar($notification->name, $notification->avatar)
                                                </div>
                                                <div class="media-body">
                                                    <div class="lgi-heading">
                                                        {!! $notification->name !!}
                                                    </div>
                                                    <small class="lgi-text">
                                                        {!! \Illuminate\Support\Str::limit($notification->message, 75) !!}
                                                    </small>
                                                </div>
                                            </a>
                                        @endforeach
                                    </div>
                                    @if($notifications->count() > 5)
                                        <a href="{!! route('core.mailer.index') !!}" class="view-more">
                                            @lang('core_system_lang::dashboard.header.notification_view_all')
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </li>
                    @endif
                @endif
                <li class="dropdown">
                    <a data-toggle="dropdown" href=""><i class="him-icon zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li class="hidden-xs">
                            <a data-ma-action="fullscreen" href="">
                                <i class="zmdi zmdi-fullscreen"></i>
                                @lang('core_system_lang::dashboard.header.settings.full_screen')
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('cms.settings.index') !!}">
                                <i class="zmdi zmdi-settings"></i> @lang('core_system_lang::dashboard.header.settings.edit')
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('auth.login.logout') !!}">
                                <i class="zmdi zmdi-power"></i> @lang('core_system_lang::dashboard.header.settings.logout')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</header>